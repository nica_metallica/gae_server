import os
import re
import random
import hashlib
import hmac
from string import letters
from datetime import datetime
import time
from google.appengine.api import mail
from google.appengine.api import images
#from time import gmtime, strftime

import webapp2
import jinja2

from google.appengine.ext import db

import model


template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir),
                               autoescape = True)

secret="Metallica"

def make_secure_val(val):
    return '%s|%s' % (val, hmac.new(secret, val).hexdigest())

def check_secure_val(secure_val):
    val = secure_val.split('|')[0]
    if secure_val == make_secure_val(val):
        return val

def render_str(template, **params):
    t = jinja_env.get_template(template)
    return t.render(params)

class BaseHandler(webapp2.RequestHandler):
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
#        params['user'] = self.user
        return render_str(template, **params)

    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))

    def logout(self):
        self.response.headers.add_header('Set-Cookie', 'username=; Path=/')

#    def initialize(self, *a, **kw):
#        webapp2.RequestHandler.initialize(self, *a, **kw)
#        uid = self.read_secure_cookie('user_id')
#        self.user = uid and model.User.by_id(int(uid))

    def login(self, user):
        self.set_secure_cookie(user)
        


    def set_secure_cookie(self, name):
        #cookie_val = make_secure_val(val)
        self.response.headers.add_header(
            'Set-Cookie',
            'username=%s; Path=/' % str(name))

    def read_secure_cookie(self, name):
        cookie_val = ""
        if not self.request.cookies.get(name)==None:
            cookie_val = str(self.request.cookies.get(name))
        return cookie_val

    def check_admin(self,name):
        u=db.GqlQuery('SELECT * FROM User WHERE name=:1',name).get()
        return u and (u.auth == 0)

class Login(BaseHandler):
    def get(self):
       self.render('receive.html')
       
    def post(self):
        try:
            phonenum = self.request.get('phonenum')
            user = model.User.all().filter('phonenum =',phonenum).get()
        
            if user:
                if user.avatar:
                    self.render('main1.html',usercheck='yy')
                else:
                    self.render('main1.html',usercheck='yn')
            else:
                self.render('main1.html',usercheck='n')
        except (RuntimeError, TypeError, NameError):
            self.render('main1.html',usercheck='some error from the server')

class Signup(BaseHandler):
    def get(self):
        self.render('receive.html')
        
    def post(self):
        try:
            phonenum = self.request.get('phonenum')
            # need user check
            username = self.request.get('username')
            temp = model.User(phonenum = phonenum, username = username)
            temp.put()
            self.render('main1.html', signupcheck ='y')
        except (RuntimeError, TypeError, NameError):
            self.render('main1.html',usercheck='some error from the server')

class AvatarUpload(BaseHandler):
    def get(self):
        self.render('receive.html')
    def post(self):
        try:
            phonenum = self.request.get('phonenum')
            avatar = self.request.get('avatar')
            temp = db.GqlQuery('SELECT * FROM User WHERE phonenum = :1',phonenum)[0]
            temp.avatar = avatar
            temp.put()
            self.render('main1.html', uploadcheck = 'y')
        except (RuntimeError, TypeError, NameError):
            self.render('main1.html',usercheck='some error from the server')

class ViewAvatar(BaseHandler):
    def get(self):
        self.render('receive.html')
    def post(self):
        try:
            phonenum = self.request.get('phonenum')
            temp = db.GqlQuery('SELECT * FROM User WHERE phonenum = :1',phonenum)[0]
            if temp.avatar:
                self.render('main1.html', viewavatarcheck = '', avatar = temp.avatar)
            else:
                self.render('main1.html', viewavatarcheck = 'n', avatar = None)
        except (RuntimeError, TypeError, NameError):
            self.render('main1.html',usercheck='some error from the server')

class CreateCircle(BaseHandler):
    def get(self):
        self.render('receive.html')
    def post(self):
        try:
            creator = self.request.get('phonenum')
            circlename = self.request.get('circlename')
            # need cname check
            temp = model.Circle(creator = creator, cname = circlename)
            cid = model. next_circle_id()
            temp.cid = cid
            temp.put()
            tempuser = db.GqlQuery('SELECT * FROM User WHERE phonenum = :1',creator)[0]
            tempuser.circle = temp
            tempuser.put()
            self.render('main1.html',createcheck='y')
        except (RuntimeError, TypeError, NameError):
            self.render('main1.html',usercheck='some error from the server')

class Invite(BaseHandler):
    def get(self):
        self.render('receive.html')
    def post(self):
        try:
            phonenum = self.request.get('invitephonenum')
            cid = int(self.request.get('cid'))
            # check phonenum in the circle
            # check phonenum alr registered
            tempuser = db.GqlQuery('SELECT * FROM User WHERE phonenum = :1',phonenum)[0]
            temp = model.InviteList(user = tempuser, invitecid = cid)
            temp.put()
            self.render('main1.html', invitecheck = 'y', invitephonenum = phonenum)
        except (RuntimeError, TypeError, NameError):
            self.render('main1.html',usercheck='some error from the server')

class InviteResponse(BaseHandler):
    def get(self):
        self.render('receive.html')
    def post(self):
        try:
            invitephonenum = self.request.get('invitephonenum')
            cid = int(self.request.get('cid'))
            tempuser = db.GqlQuery('SELECT * FROM User WHERE phonenum = :1',invitephonenum)[0]
            temp = db.GqlQuery('SELECT * FROM InviteList WHERE user = :1 AND invitecid = :2',tempuser,cid)[0]
            db.delete(temp)
            inviteresponse = self.request.get('inviteresponse')
            if inviteresponse == 'y':
                tempuser = db.GqlQuery('SELECT * FROM User WHERE phonenum = :1',invitephonenum)[0]
                temp = db.GqlQuery('SELECT * FROM Circle WHERE cid = :1',cid)[0]
                tempuser.circle = temp
                tempuser.put()
                self.render('main1.html', inviteresponsecheck = 'y',inviteresponse = inviteresponse)
            
            else:
                self.render('main1.html', inviteresponsecheck = 'n',inviteresponse = inviteresponse)
        except (RuntimeError, TypeError, NameError):
            self.render('main1.html',usercheck='some error from the server')


class ImageUpload(BaseHandler):
    def get(self):
        self.render('receive.html')
    def post(self):
        try:
            phonenum = self.request.get('phonenum')
            image = self.request.get('image')
            tempuser = db.GqlQuery('SELECT * FROM User WHERE phonenum = :1',phonenum)[0]
            temp = model.Image(user = tempuser, img_content = image)
            temp.imgid = model.next_image_id()
            temp.put()
            self.render('main1.html', imageuploadcheck = 'y')
        except (RuntimeError, TypeError, NameError):
            self.render('main1.html',usercheck='some error from the server')

class ViewCircleMembers(BaseHandler):
    def get(self):
        self.render('receive.html')
    def post(self):
        try:
            phonenum = self.request.get('phonenum')
            cid = int(self.request.get('cid'))
            circle = db.GqlQuery('SELECT * FROM Circle WHERE cid = :1',cid)[0]
            memberlist = []
            for member in circle.cmembers:
                memberlist.append(member.phonenum)
            self.render('main1.html',memberlist = memberlist)
        except (RuntimeError, TypeError, NameError):
            self.render('main1.html',usercheck='some error from the server')

class ViewImage(BaseHandler):
    def get(self):
        self.render('receive.html')
    def post(self):
        try:
            imgid = int(self.request.get('imageid'))
            image = db.GqlQuery('SELECT * FROM Image WHERE imgid = :1',imgid)[0]
            imageinfo = [image.user.username, image.date, image.img_content]
            self.render('main1.html',image = imageinfo)
        except (RuntimeError, TypeError, NameError):
            self.render('main1.html',usercheck='some error from the server')

class ImagelistGenerate(BaseHandler):
    def get(self):
        self.render('receive.html')
    def post(self):
        try:
            phonenum = self.request.get('phonenum')
            circle = db.GqlQuery('SELECT * FROM User WHERE phonenum = :1',phonenum)[0].circle
            imagelist = []
            if circle:
                for member in circle.cmembers:
                    for image in member.image_list:
                        imagelist.append(int(image.imgid))
                self.render('main1.html',image = imagelist)
            else:
                self.render('main1.html',image = 'You are not in any circles')
        except (RuntimeError, TypeError, NameError, IOError):
            self.render('main1.html',usercheck='some error from the server')

class PresentCircle(BaseHandler):
    def get(self):
        phonenum=self.request.get('pn')   #phone number
        user=db.GqlQuery('SELECT * FROM User WHERE phonenum = :1', phonenum).get()
        if not user:
            self.response.out.write('nc')
        else:
            self.render('clrinfo.html',members=user.circle.cmembers)

            
app = webapp2.WSGIApplication([('/', Login),
                               ('/signup', Signup),
                               ('/avatarupload', AvatarUpload),
                               ('/createcircle', CreateCircle),
                               ('/invite', Invite),
                               ('/inviteresponse', InviteResponse),
                               ('/viewavatar', ViewAvatar),
                               ('/imageupload', ImageUpload),
                               ('/imagelistgenerate', ImagelistGenerate),
                               ('/viewimage', ViewImage),
                               ('/viewcirclemembers',ViewCircleMembers),
                               ('/circleinfo', PresentCircle),
                                 ],
                               debug=True)       


