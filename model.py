from google.appengine.ext import db

class Circle(db.Model):
    cid = db. IntegerProperty()
    creator = db. StringProperty()
    cname = db. StringProperty()

class User(db.Model):
    phonenum = db. StringProperty()
    username = db. StringProperty()
    avatar = db. TextProperty()
    circle = db. ReferenceProperty(Circle, collection_name = 'cmembers')

class Gps(db.Model):
    user = db. ReferenceProperty(User, collection_name = 'gps_record')
    date = db. DateTimeProperty(auto_now_add = True)
    x_cord = db. StringProperty()
    y_cord = db. StringProperty()

class Image(db.Model):
    user = db. ReferenceProperty(User, collection_name = 'image_list')
    date = db. DateTimeProperty(auto_now_add = True)
    img_content = db. TextProperty()
    imgid = db. IntegerProperty()

class InviteList(db.Model):
    user = db. ReferenceProperty(User, collection_name = 'invite_list')
    invitecid = db. IntegerProperty()

def next_circle_id():
    circle=db.GqlQuery('SELECT * FROM Circle ORDER BY cid DESC')
    return 0 if circle.count() == 0 else circle[0].cid+1
def next_image_id():
    image=db.GqlQuery('SELECT * FROM Image ORDER BY imgid DESC')
    return 0 if image.count() == 0 else image[0].imgid+1
